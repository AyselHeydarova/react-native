import React, { useState, useEffect,createContext } from 'react';

export const ShipContext = createContext();

export const ShipContextProvider = ({children}) => {
        const [shipsList, setShipsList] = useState([]);
        console.log(shipsList);

        const getShipsList = async () => {
            const list = await getAllships();
            console.log(list)
            setShipsList(list);

        
        }

        useEffect(() => {
            getShipsList();
        },[] )


        return (
            <ShipContext.Provider value={{shipsList}}>
                {children}
            </ShipContext.Provider>
        )
}

function  getAllships() {
    let ships = [];
    let link = 'https://swapi.py4e.com/api/starships';

    return new Promise (async (resolve)=> {
        while(link) {
            try{
                 let response = await fetch(link);
                let data = await response.json();
                ships = [...ships, ...data.results]
                link = data.next;
            }
            catch {
                console.log(error);
                link = null
            }
        }
       resolve(ships) 
        
    } 

    )
}
