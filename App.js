import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import HomeScreen from './Screens/HomeScreen';
import { ShipContextProvider } from './Components/ShipContext';

export default function App() {
  return (
   <ShipContextProvider>
       <HomeScreen/>
   </ShipContextProvider>
  
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
