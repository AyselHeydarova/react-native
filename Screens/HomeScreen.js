import React, { useContext } from 'react';
import {StyleSheet, View, Text, FlatList} from 'react-native';
import { ShipContextProvider, ShipContext } from '../Components/ShipContext';
import ListCardItem from './ListCardItem';


const HomeScreen = () => {

    const {shipsList} = useContext(ShipContext);

    return (
        <ShipContextProvider>
             <View style={styles.container}>
                <Text>This is a HomeeScreen</Text>
                <FlatList  
                data={shipsList}
                numColumns={2}
                renderItem={ ({item}) => (<ListCardItem name={item.name} cost={item.cost_in_credits}/>)}
            />
  
             </View>
        </ShipContextProvider>
       
    );
};

const styles = StyleSheet.create({
      container: {
        flex: 1,
        justifyContent: "center",
        alignItems: "center",
    }
})

export default HomeScreen;