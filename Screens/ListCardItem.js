import React from 'react';
import {Text, View, StyleSheet, TouchableWithoutFeedback, TouchableOpacity} from 'react-native';
import SingleShipPage from './SingleShipPage';


const ListCardItem = (props) => {

    return (
        <TouchableOpacity style={styles.container} onPress={()=> (<SingleShipPage/>)}>
            <View >
            <Text style={styles.name}>{props.name}</Text>
            <Text style={styles.cost}>{props.cost}</Text>

        </View>
        </TouchableOpacity>
        
    

    )
}

const styles = StyleSheet.create({
    container: {
      
        backgroundColor : 'teal',
        marginVertical : 10,
        marginHorizontal : 10,
        width: '45%',
        height: 90,
        justifyContent:"center",
        borderRadius: 20,
        paddingHorizontal: 10
    },

    name: {
        color:'white',
        

    },

    cost: {
        fontWeight: 'bold',
        color: "white",
        justifyContent: "flex-end"
    }
}

)

export default ListCardItem;